//
//  OsirnijeNews.swift
//  AndroidTabs
//
//  Created by Amplitudo on 26/01/2019.
//  Copyright © 2019 Amplitudo. All rights reserved.
//

import Foundation
struct OpsirnijeNews {
    private(set) public var title: String
    private(set) public var date: String
    private(set) public var content: String
    private(set) public var image: String
    
    init(title: String, image: String, date: String, content: String){
        self.title = title
        self.image = image
        self.date = date
        self.content = content
    }
}
