//
//  Announcement.swift
//  AndroidTabs
//
//  Created by Amplitudo on 21/01/2019.
//  Copyright © 2019 Amplitudo. All rights reserved.
//

import Foundation
struct Announcement {
    private(set) public var title:String
    private(set) public var image:String
    private(set) public var date:String
    
    init(title: String, image:String, date:String){
        self.title = title
        self.image = image
        self.date = date
    }
}
