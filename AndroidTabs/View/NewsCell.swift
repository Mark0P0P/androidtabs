//
//  NewsCell.swift
//  AndroidTabs
//
//  Created by Amplitudo on 21/01/2019.
//  Copyright © 2019 Amplitudo. All rights reserved.
//

import UIKit

class NewsCell: UITableViewCell {

    
    @IBOutlet weak var newsImage: UIImageView!
    @IBOutlet weak var newsTitle: UILabel!
    @IBOutlet weak var newsContent: UILabel!
    @IBOutlet weak var newsDate: UILabel!
    
    func updateViewsNews(news: Article){
        let myURLString: String = news.image!
        let myURL = URL(string: myURLString)
        newsImage.kf.setImage(with:myURL)
        //newsImage.backgroundColor = .red
        newsTitle.text = news.title
        newsDate.text = news.date
    }
    
    
    //override func awakeFromNib() {
    //  super.awakeFromNib()
    // Initialization code
    //}
    
    //override func setSelected(_ selected: Bool, animated: Bool) {
    //  super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
    //}
    
}
