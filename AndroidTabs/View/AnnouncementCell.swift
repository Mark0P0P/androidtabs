//
//  AnnouncementCell.swift
//  AndroidTabs
//
//  Created by Amplitudo on 21/01/2019.
//  Copyright © 2019 Amplitudo. All rights reserved.
//

import UIKit

class AnnouncementCell: UITableViewCell {

    @IBOutlet weak var announcementImage: UIImageView!
    @IBOutlet weak var announcementTitle: UILabel!
    @IBOutlet weak var announcementContent: UILabel!
    @IBOutlet weak var announcementDate: UILabel!
    
    func updateViewsAnnouncement(announcement: Article){
        let myURLString: String = announcement.image!
        let myURL = URL(string: myURLString)
        announcementImage.kf.setImage(with:myURL)
        announcementTitle.text = announcement.title
        announcementDate.text = announcement.date

}
}
