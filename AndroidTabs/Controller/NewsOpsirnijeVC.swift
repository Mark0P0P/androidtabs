//
//  NewsOpsirnijeVC.swift
//  AndroidTabs
//
//  Created by Amplitudo on 26/01/2019.
//  Copyright © 2019 Amplitudo. All rights reserved.
//

import UIKit

class NewsOpsirnijeVC: UIViewController {
    
    
    
    @IBOutlet weak var opsirnijeNewsTitle: UILabel!
    @IBOutlet weak var opsirnijeNewsDate: UILabel!
    @IBOutlet weak var opsirnijeNewsContent: UITextView!
    @IBOutlet weak var opsirnijeNewsImage: UIImageView!
    @IBOutlet weak var opsirnijeNewsScroll: UIScrollView!
    
    var newsArticle: Article!
    
    var name = ""
    var date = ""
    var content = ""
    var image = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
      opsirnijeNewsTitle.text = name
        opsirnijeNewsDate.text = date
        opsirnijeNewsContent.text = content
        opsirnijeNewsImage.image = image
        //opsirnijeNewsScroll.contentLayoutGuide.bottomAnchor.constraint(equalTo: opsirnijeNewsContent.bottomAnchor).isActive = true
        print(newsArticle.title)
    }

}

/*
var name = ""
var date = ""
var content = ""
var image = UIImage()



override func viewDidLoad() {
    super.viewDidLoad()
    detailTitleNews.text = name
    detailDateNews.text = date
    detailContentNews.text = content
    detailImageNews.image = image
    detailScrollNews.contentLayoutGuide.bottomAnchor.constraint(equalTo: detailContentNews.bottomAnchor).isActive = true
}
*/
