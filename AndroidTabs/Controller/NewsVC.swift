//
//  ChildVC1.swift
//  AndroidTabs
//
//  Created by Amplitudo on 20/01/2019.
//  Copyright © 2019 Amplitudo. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Kingfisher

class NewsVC: UIViewController, UITableViewDataSource, UITableViewDelegate, IndicatorInfoProvider {
    
    @IBOutlet weak var newsTable: UITableView!
    var parentVC: ParentVC!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.newsTable.rowHeight = 320;

        newsTable.dataSource = self
        newsTable.delegate = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return OzonDataService.instance.getNews().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCell") as? NewsCell{
            let news = OzonDataService.instance.getNews()[indexPath.row]
            cell.updateViewsNews(news: news)
            return cell
        }   else {
            return NewsCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NewsOpsirnijeVC") as! NewsOpsirnijeVC
        vc.newsArticle = OzonDataService.instance.getNews()[indexPath.row]
        vc.name = OzonDataService.instance.getNews()[indexPath.row].title
        vc.date = OzonDataService.instance.getNews()[indexPath.row].date
        vc.content = OzonDataService.instance.getNews()[indexPath.row].content
        
        let myURL = URL(string: OzonDataService.instance.getNews()[indexPath.row].image!)
        KingfisherManager.shared.retrieveImage(with: myURL!, options: nil, progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
            vc.image = image!
        })

        parentVC.navigationController?.pushViewController(vc, animated: true)
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Vijesti")
    }
    
    
    
}
