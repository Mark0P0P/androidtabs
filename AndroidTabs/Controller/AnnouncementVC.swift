//
//  ChildVC2.swift
//  AndroidTabs
//
//  Created by Amplitudo on 20/01/2019.
//  Copyright © 2019 Amplitudo. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Kingfisher

class AnnouncementVC: UIViewController, UITableViewDataSource, UITableViewDelegate, IndicatorInfoProvider {
    
    @IBOutlet weak var announcementTable: UITableView!
    var parentVC: ParentVC!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.announcementTable.rowHeight = 320;
        
        announcementTable.dataSource = self
        announcementTable.delegate = self
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return OzonDataService.instance.getAnnouncement().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "AnnouncementCell") as? AnnouncementCell{
            let announcement = OzonDataService.instance.getAnnouncement()[indexPath.row]
            cell.updateViewsAnnouncement(announcement: announcement)
            return cell
        }   else {
            return AnnouncementCell()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementsOpsirnijeVC")
        parentVC.navigationController?.pushViewController(vc, animated: true)
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Obavjestenja")
    }
   
}
